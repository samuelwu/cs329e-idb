import os
import sys
import unittest
from models import db, Book, getVal
from main import sortby

class DBTestCases(unittest.TestCase):
    def test_source_insert_1(self):
        s = Book(google_id="wrOQLV6xB-wC", title = "Harry Potter and the Sorcerer's Stone Test", isbn = "9781781100486",
        publication_date = "2015-12-08",
        image_url = "https://books.google.com/books/content/images/frontcover/wrOQLV6xB-wC?fife=w500",
        description = "\"Turning the envelope over, his hand trembling, Harry saw a purple wax seal bearing a coat of arms; a lion, an eagle, a badger and a snake surrounding a large letter 'H'.\" Harry Potter has never even heard of Hogwarts when the letters start dropping on the doormat at number four, Privet Drive. Addressed in green ink on yellowish parchment with a purple seal, they are swiftly confiscated by his grisly aunt and uncle. Then, on Harry's eleventh birthday, a great beetle-eyed giant of a man called Rubeus Hagrid bursts in with some astonishing news: Harry Potter is a wizard, and he has a place at Hogwarts School of Witchcraft and Wizardry. An incredible adventure is about to begin!",
        publishers = "Pottermore",
        authors = "J. K. Rowling")
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(title = "Harry Potter and the Sorcerer's Stone Test").one()
        self.assertEqual(str(r.title), "Harry Potter and the Sorcerer's Stone Test")

        db.session.query(Book).filter_by(title = "Harry Potter and the Sorcerer's Stone").delete()
        db.session.commit()
    
    def test_source_insert_2(self):
        s = Book(google_id = "qtGvAwAAQBAJ",
        title = "The Hero of Ages Test",
        isbn = "9780765356147",
        publication_date = "2009-04-28",
        image_url = "https://books.google.com/books/content/images/frontcover/qtGvAwAAQBAJ?fife=w500",
        description = "Emperor Elend Venture, having survived only to become a Mistborn himself, struggles to find clues by the Lord Ruler that will save his world, while a guilt-consumed Vin takes on a task of ending the cosmic power of the Ruin mystic force.",
        publishers = "Palgrave Macmillan",
        authors = "Brandon Sanderson")
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(title = "The Hero of Ages Test").one()
        self.assertEqual(str(r.title),  "The Hero of Ages Test")

        db.session.query(Book).filter_by(title = "The Hero of Ages Test").delete()
        db.session.commit()
    
    def test_source_insert_3(self):
        s = Book(google_id = "hgx0sJvphNkC",
        title = "The Emperor of All Maladies Test",
        isbn = "9781439170915",
        publication_date = "2011-08-09",
        image_url = "https://books.google.com/books/content/images/frontcover/hgx0sJvphNkC?fife=w500",
        description = "An assessment of cancer addresses both the courageous battles against the disease and the misperceptions and hubris that have compromised modern understandings, providing coverage of such topics as ancient-world surgeries and the development of present-day treatments. Reprint. Best-selling winner of the Pulitzer Prize. Includes reading-group guide.",
        publishers = "Simon & Schuster",
        authors = "Siddhartha Mukherjee")
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(title = "The Emperor of All Maladies Test").one()
        self.assertEqual(str(r.title), "The Emperor of All Maladies Test")

        db.session.query(Book).filter_by(title = "The Emperor of All Maladies Test").delete()
        db.session.commit()

    def test_Book_getVal1(self):
        s = {"google_id" : "hgx0sJvphNkC",
        "title" : "The Emperor of All Maladies",
        "isbn" : "9781439170915",
        "publication_date" : "2011-08-09",
        "image_url" : "https://books.google.com/books/content/images/frontcover/hgx0sJvphNkC?fife=w500",
        "description" : "An assessment of cancer addresses both the courageous battles against the disease and the misperceptions and hubris that have compromised modern understandings, providing coverage of such topics as ancient-world surgeries and the development of present-day treatments. Reprint. Best-selling winner of the Pulitzer Prize. Includes reading-group guide.",
        "publishers" : "Simon & Schuster",
        "authors" : "Siddhartha Mukherjee"}

        a = getVal(s, "google_id")
        self.assertEqual(a, "hgx0sJvphNkC")

    def test_Author_getVal2(self):
        s = {"name" : "Aldo Villarreal",
        "education" : "Mathematics",
        "alma_mater" : "University of Texas",
        "nationality" : "Mexican-American",
        "born" : "May 31, 1998",
        "image_url" : "image",
        "description" : "Boy that made this unit test",
        "wikipedia_url" : "url",
        "publishers" : "GrandMarc",
        "books" : "Aldo's Blog"}

        a = getVal(s, "born")
        self.assertEqual(a, "May 31, 1998")

    def test_Publisher_getVal3(self):
        s = {"name" : "Aldo Publishing",
        "owner" : "Aldo",
        "parent_company" : "Suruchi",
        "website" : "Placement URL",
        "image_url" : "Placement Image",
        "description" : "Description",
        "wikipedia_url" : "Placement URL",
        "books" : "Book1",
        "authors" : "Aldo & Friends"}

        a = getVal(s, "website")
        self.assertEqual(a, "Placement URL")

    def test_sortby1(self):
        book_list = []
        t = Book(google_id = "qtGvAwAAQBAJ",
        title = "The Hero of Ages",
        isbn = "9780765356147",
        publication_date = "2009-04-28",
        image_url = "https://books.google.com/books/content/images/frontcover/qtGvAwAAQBAJ?fife=w500",
        description = "Emperor Elend Venture, having survived only to become a Mistborn himself, struggles to find clues by the Lord Ruler that will save his world, while a guilt-consumed Vin takes on a task of ending the cosmic power of the Ruin mystic force.",
        publishers = "Palgrave Macmillan",
        authors = "Brandon Sanderson")

        book_list.append(t)

        s = Book(google_id = "hgx0sJvphNkC",
        title = "The Emperor of All Maladies",
        isbn = "9781439170915",
        publication_date = "2011-08-09",
        image_url = "https://books.google.com/books/content/images/frontcover/hgx0sJvphNkC?fife=w500",
        description = "An assessment of cancer addresses both the courageous battles against the disease and the misperceptions and hubris that have compromised modern understandings, providing coverage of such topics as ancient-world surgeries and the development of present-day treatments. Reprint. Best-selling winner of the Pulitzer Prize. Includes reading-group guide.",
        publishers = "Simon & Schuster",
        authors = "Siddhartha Mukherjee")

        book_list.append(s)

        u = Book(google_id = "qtGvAwABZBAJ",
        title = "Book Test",
        isbn = "9201946356147",
        publication_date = "2009-5-28",
        image_url = "Placeholder",
        description = "Placeholder Description",
        publishers = "Aldo Publishing",
        authors = "Aldo Villarreal")

        book_list.append(u)

        book_list = sortby(book_list, "title", 1)
        self.assertEqual(type(book_list), list)

    def test_sortby2(self):
        book_list = []
        t = Book(google_id = "qtGvAwAAQBAJ",
        title = "The Hero of Ages",
        isbn = "9780765356147",
        publication_date = "2009-04-28",
        image_url = "https://books.google.com/books/content/images/frontcover/qtGvAwAAQBAJ?fife=w500",
        description = "Emperor Elend Venture, having survived only to become a Mistborn himself, struggles to find clues by the Lord Ruler that will save his world, while a guilt-consumed Vin takes on a task of ending the cosmic power of the Ruin mystic force.",
        publishers = "Palgrave Macmillan",
        authors = "Brandon Sanderson")

        book_list.append(t)

        s = Book(google_id = "hgx0sJvphNkC",
        title = "The Emperor of All Maladies",
        isbn = "9781439170915",
        publication_date = "2011-08-09",
        image_url = "https://books.google.com/books/content/images/frontcover/hgx0sJvphNkC?fife=w500",
        description = "An assessment of cancer addresses both the courageous battles against the disease and the misperceptions and hubris that have compromised modern understandings, providing coverage of such topics as ancient-world surgeries and the development of present-day treatments. Reprint. Best-selling winner of the Pulitzer Prize. Includes reading-group guide.",
        publishers = "Simon & Schuster",
        authors = "Siddhartha Mukherjee")

        book_list.append(s)

        u = Book(google_id = "qtGvAwABZBAJ",
        title = "Book Test",
        isbn = "9201946356147",
        publication_date = "2009-5-28",
        image_url = "Placeholder",
        description = "Placeholder Description",
        publishers = "Aldo Publishing",
        authors = "Aldo Villarreal")

        book_list.append(u)

        book_list = sortby(book_list, "title", 1)
        self.assertEqual(book_list[0][0].title, "Book Test")

    def test_sortby3(self):
        book_list = []
        t = Book(google_id = "qtGvAwAAQBAJ",
        title = "The Hero of Ages",
        isbn = "9780765356147",
        publication_date = "2009-04-28",
        image_url = "https://books.google.com/books/content/images/frontcover/qtGvAwAAQBAJ?fife=w500",
        description = "Emperor Elend Venture, having survived only to become a Mistborn himself, struggles to find clues by the Lord Ruler that will save his world, while a guilt-consumed Vin takes on a task of ending the cosmic power of the Ruin mystic force.",
        publishers = "Palgrave Macmillan",
        authors = "Brandon Sanderson")

        book_list.append(t)

        s = Book(google_id = "hgx0sJvphNkC",
        title = "The Emperor of All Maladies",
        isbn = "9781439170915",
        publication_date = "2011-08-09",
        image_url = "https://books.google.com/books/content/images/frontcover/hgx0sJvphNkC?fife=w500",
        description = "An assessment of cancer addresses both the courageous battles against the disease and the misperceptions and hubris that have compromised modern understandings, providing coverage of such topics as ancient-world surgeries and the development of present-day treatments. Reprint. Best-selling winner of the Pulitzer Prize. Includes reading-group guide.",
        publishers = "Simon & Schuster",
        authors = "Siddhartha Mukherjee")

        book_list.append(s)

        u = Book(google_id = "qtGvAwABZBAJ",
        title = "Book Test",
        isbn = "9201946356147",
        publication_date = "2009-5-28",
        image_url = "Placeholder",
        description = "Placeholder Description",
        publishers = "Aldo Publishing",
        authors = "Aldo Villarreal")

        book_list.append(u)

        book_list = sortby(book_list, "title", 1)
        self.assertEqual(book_list[2][0].title, "The Hero of Ages")

if __name__ == '__main__':
    unittest.main()