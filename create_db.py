# beginning of create_db.py
import json
from models import db, Book, Author, Publisher
def load_json(filename):
	with open(filename) as file:
		jsn = json.load(file)
		file.close()
	return jsn

def create_books():
	book = load_json('books.json')
	for oneBook in book:
		try:
			title = oneBook['title']
		except KeyError:
			title = "N/A"
		try: 
			google_id = oneBook['google_id']
		except KeyError:
			google_id = "N/A"
		try:
			isbn = oneBook['isbn']
		except KeyError:
			isbn = "N/A"
		try:
			publication_date = oneBook['publication_date']
		except KeyError:
			publication_date = "N/A"
		try:
			image_url = oneBook['image_url']
		except KeyError:
			image_url = "N/A"
		try:
			description = oneBook['description']
		except KeyError:
			description = "N/A"
		publishers = oneBook['publishers'][0]['name']
		authors = oneBook['authors'][0]['name']

		newBook = Book(title = title, google_id = google_id, isbn = isbn, publication_date = publication_date, image_url = image_url, description = description, publishers = publishers, authors = authors)

	 # After I create the book, I can then add it to my session.
		db.session.add(newBook)
	 # commit the session to my DB.
		db.session.commit()

def create_authors():
	book = load_json('books.json')
	for oneBook in book:
		try:
			born = oneBook['authors'][0]['born']
		except KeyError:
			born = "N/A"
		try: 
			name = oneBook['authors'][0]['name']
		except KeyError:
			name = "N/A"
		try:
			education = oneBook['authors'][0]['education']
		except KeyError:
			education = "N/A"
		try:
			nationality = oneBook['authors'][0]['nationality']
		except KeyError:
			nationality = "N/A"
		try:
			image_url = oneBook['authors'][0]['image_url']
		except KeyError:
			image_url = "N/A"
		try:
			wikipedia_url = oneBook['authors'][0]['wikipedia_url']
		except KeyError:
			wikipedia_url = "N/A"
		try:
			description = oneBook['authors'][0]['description']
		except KeyError:
			description = "N/A"
		try:
			alma_mater = oneBook['authors'][0]['alma_mater']
		except KeyError:
			alma_mater = "N/A"
		publishers = oneBook['publishers'][0]['name']
		title = oneBook['title']

		newBook = Author(born = born, name = name, education = education, nationality = nationality, image_url = image_url, publishers = publishers, title = title, wikipedia_url = wikipedia_url, description = description, alma_mater = alma_mater)

	 # After I create the book, I can then add it to my session.
		db.session.add(newBook)
	 # commit the session to my DB.
		db.session.commit()

def create_publishers():
	book = load_json('books.json')
	for oneBook in book:
		authors = oneBook['authors'][0]['name']
		title = oneBook['title']
		try: 
			name = oneBook['publishers'][0]['name']
		except KeyError:
			name = "N/A"
		try:
			owner = oneBook['publishers'][0]['owner']
		except KeyError:
			owner = "N/A"
		try:
			parent_company = oneBook['publishers'][0]['parent company']
		except KeyError:
			parent_company = "N/A"
		try:
			website = oneBook['publishers'][0]['website']
		except KeyError:
			website = "N/A"
		try:
			image_url = oneBook['publishers'][0]['image_url']
		except KeyError:
			image_url = "N/A"
		try:
			wikipedia_url = oneBook['publishers'][0]['wikipedia_url']
		except KeyError:
			wikipedia_url = "N/A"
		try:
			description = oneBook['publishers'][0]['description']
		except KeyError:
			description = "N/A"

		newBook = Publisher(name = name, owner = owner, parent_company = parent_company, image_url = image_url, title = title, wikipedia_url = wikipedia_url, description = description, authors = authors, website = website)

	 # After I create the book, I can then add it to my session.
		db.session.add(newBook)
	 # commit the session to my DB.
		db.session.commit()

create_books()
create_authors()
create_publishers()
# end of create_db.py