#-----------------------------------------
# main2.py
# creating first flask application
#-----------------------------------------
import subprocess
from flask import Flask, render_template
from models import app, db, Book, Author, Publisher
from create_db import create_books

#app = Flask(__name__)

# This is the main splash page
@app.route('/')
def index():
	return render_template('index.html')

# This is the about page
@app.route('/about/')
def about():
	return render_template('about.html')

# These next three list all the instances of books, authors, and publishers
@app.route('/allbooks/') #allows allbooks to render without an argument
@app.route('/allbooks/<key>/<int:page>/<int:sort>')
def allbooks(key = 'title', page = 0,sort = 1): # default page number is 0
	books = db.session.query(Book).order_by(Book.title).all()
	books = sortby(books,key,sort)
	return render_template('allbooks.html', books = books, key = key, page = page, sort = sort)

@app.route('/allauthors/')
@app.route('/allauthors/<key>/<int:page>/<int:sort>')
def allauthors(key = 'name', page = 0,sort = 1): # default page number is 0
	authors = db.session.query(Author).order_by(Author.title).all()
	authors = sortby(authors,key,sort)
	return render_template('allauthors.html', authors = authors, key = key, page = page, sort = sort)

@app.route('/allpub/')
@app.route('/allpub/<key>/<int:page>/<int:sort>')
def allpub(key = 'name', page = 0,sort = 1): # default page number is 0
	publishers = db.session.query(Publisher).order_by(Publisher.title).all()
	publishers = sortby(publishers,key,sort)
	return render_template('allpub.html', publishers = publishers, key = key, page = page, sort = sort)

# The next functions are for static pages of books, authors, and publishers
# Books
@app.route('/book/<int:i>') # replace with @app.route('/'+str(book.google_id)+'/')
def book1(i = 0): # replace with def book1(book) # 1 initial in allb, 2 in book1
	books = db.session.query(Book).order_by(Book.title).all()
	books = sortby(books,"title",1)
	return render_template('book1.html', books = books, i = i) # return render_template('book1.html',book = book)

# Authors
@app.route('/author/<int:i>')
def author1(i = 0):
	authors = db.session.query(Author).order_by(Author.title).all()
	authors = sortby(authors,"title",1)
	return render_template('author1.html', authors = authors, i = i)

# Publishers
@app.route('/publisher/<int:i>')
def pub1(i = 0):
	publishers = db.session.query(Publisher).order_by(Publisher.title).all()
	publishers = sortby(publishers,"title",1)
	return render_template('pub1.html', publishers = publishers, i = i)

# To run the unit tests, 
@app.route('/test/')
def test():
	p = subprocess.Popen(["python", "test.py"],
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			stdin=subprocess.PIPE)
	out, err = p.communicate()
	output=err+out
	output = output.decode("utf-8") #convert from byte type to string type
	
	return render_template('test.html', output = "<br/>".join(output.split("\n")))

@app.route('/search/')
@app.route('/search/<term>')
def search(term = "default"):
	#results = []
	#term = term.upper()
	books = db.session.query(Book).order_by(Book.title).all()
	books = sortby(books,"title",1)
	authors = db.session.query(Author).order_by(Author.title).all()
	authors = sortby(authors,"title",1)
	publishers = db.session.query(Publisher).order_by(Publisher.title).all()
	publishers = sortby(publishers,"title",1)
	'''
	for i in range(len(books)):
		check = books[i][0].title.upper()
		if term in check:
			results.append(books[i][0])
		check = authors[i][0].name.upper()
		if term in check:
			results.append(authors[i][0])
		checkPub = publishers[i][0].name.upper()
		if term in check:
			results.append(publishers[i][0])
	'''
	return render_template('search.html', term = term, books = books, authors = authors, publishers = publishers)

def sortby(bookList,key,sort):
	
	# Adds 2-D list to keep indices with the books for use in static pages
	newList = [[bookList[0], 0]] ##
	index = 1
	for aBook in bookList[1:]:
		val = aBook.getVal(key)
		i = 0
		while i < len(newList):
			entry = newList[i][0].getVal(key)
			if type(entry) == str:
				entry = entry.lower()
				val = val.lower() # converting all strings to lowercase for evaluation
			if val < entry:
				newList.insert(i, [aBook, index])
				break
			i+=1
		if i >= len(newList): # the while loop never breaks
			newList.append([aBook, index]) ##
		index += 1 ##
	# iterate through dictionaries
	# insert into list in correct order based on values for given key
	

	if not sort:
		newList.reverse()

	return newList

if __name__ == "__main__":
	app.run()
#----------------------------------------
# end of main2.py
#-----------------------------------------
