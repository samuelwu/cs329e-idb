#!/usr/bin/env python

"""
# Start of Models.py
"""
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import os


#creating a flask application object and set URI for the database to be used 
app = Flask(__name__) 
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:password@/postgres?host=/cloudsql/cs329e-idb-220123:us-central1:book-db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

# Defining a function for unit tests
def getVal(dict, key):
	return dict[key]


#creating a model for the Book class- and defining various ORM functions
class Book(db.Model): 
	__tablename__ = 'book'
	
	# Creates columns for the database table
	title = db.Column(db.String, primary_key = True)
	google_id = db.Column(db.String, nullable = False)
	isbn = db.Column(db.String, nullable = False)
	publication_date = db.Column(db.String, nullable = False)
	image_url = db.Column(db.String, nullable = False)
	description = db.Column(db.String, nullable = False)
	authors = db.Column(db.String, nullable = False)
	publishers = db.Column(db.String, nullable = False)

	# Allows us to grab specific values
	def getVal(self,key):
		valDict = {'title':self.title,'google_id':self.google_id,'isbn':self.isbn,'publication_date':self.publication_date,'image_url':self.image_url, 'description':self.description,'authors':self.authors,'publishers':self.publishers}
		return valDict[key]

#creating a model for the Author class- and defining various ORM functions
class Author(db.Model): 
	__tablename__ = 'author'
	
	# Columns for database
	title = db.Column(db.String, primary_key = True)
	name = db.Column(db.String, nullable = False)
	education = db.Column(db.String, nullable = False)
	alma_mater = db.Column(db.String, nullable = False)
	nationality = db.Column(db.String, nullable = False)
	born = db.Column(db.String, nullable = False)
	image_url = db.Column(db.String, nullable = False)
	description = db.Column(db.String, nullable = False)
	wikipedia_url = db.Column(db.String, nullable = False)
	publishers = db.Column(db.String, nullable = False)

	def getVal(self,key):
		valDict = {'name':self.name,'education':self.education,'alma_mater':self.alma_mater,'nationality':self.nationality,'born':self.born,'image_url':self.image_url, 'description':self.description, 'wikipedia_url': self.wikipedia_url, 'title':self.title,'publishers':self.publishers}
		return valDict[key]

#creating a model for the Author class- and defining various ORM functions
class Publisher(db.Model): 
	__tablename__ = 'publisher'
	
	# Columns for database
	title = db.Column(db.String, primary_key = True)
	name = db.Column(db.String, nullable = False)
	owner = db.Column(db.String, nullable = False)
	parent_company = db.Column(db.String, nullable = False)
	website = db.Column(db.String, nullable = False)
	image_url = db.Column(db.String, nullable = False)
	description = db.Column(db.String, nullable = False)
	wikipedia_url = db.Column(db.String, nullable = False)
	authors = db.Column(db.String, nullable = False)

	def getVal(self,key):
		valDict = {'name':self.name,'owner':self.owner,'parent_company':self.parent_company,'website':self.website,'image_url':self.image_url, 'description':self.description, 'wikipedia_url': self.wikipedia_url, 'title':self.title,'authors':self.authors}
		return valDict[key]

db.drop_all()
db.create_all()
# End of models.py